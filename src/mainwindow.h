#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QGraphicsScene>
#include "qlinepopsolver.h"
#include "monkeyrunnerprocess.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_monkeyRunnerButton_clicked();
    void updatedImage(QImage image);


private:
    Ui::MainWindow *ui;
    QString m_monkeyRunnerApplicationPath;
    QString m_monkeyRunnerOutput;
    LinePopSolver* m_linePopSolver;
    QPixmap pixmap;
    QGraphicsScene scene;
};

#endif // MAINWINDOW_H
