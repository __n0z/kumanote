#include "qlinepopsolver.h"
#include <QTime>
#include <QDebug>
#include <QPoint>
#include <QList>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "koma.h"

LinePopSolver::LinePopSolver(QObject *parent) :
    QObject(parent)
{
    m_timer = new QTimer();
    m_timer->setInterval(MAKE_COMMAND_INTERVAL);
//    connect(m_timer, SIGNAL(timeout()), this, SLOT(makeCommand()));

    m_directions << "Up" << "Down" << "Left" << "Right";
    srand(QDateTime::currentMSecsSinceEpoch());

    m_monkeyRunnerProcess = new MonkeyRunnerProcess();
    connect(m_monkeyRunnerProcess, SIGNAL(started()), this, SLOT(startSolver()));
//    connect(m_monkeyRunnerProcess, SIGNAL(updatedImage(QString)), parent, SLOT(updatedImage(QString)));
    connect(m_monkeyRunnerProcess, SIGNAL(updatedImage(QString)), this, SLOT(updatedImage(QString)));
}

void LinePopSolver::takeSnap() {
    static int count = 0;

    QString imageFilename = QString("%1.png").arg(count);
    QString command = "";

    command = command + QString("image = device.takeSnapshot()\n");
    command = command + QString("image.writeToFile(\"%1.png\")\n").arg(count);
    command = command + QString("print (\"image update : %1.png\")\n").arg(count);
    count++;
    m_monkeyRunnerProcess->write(command.toAscii());
    qDebug() << command;

}

void LinePopSolver::swap(int x, int y, QString direction) {
    qDebug() << x << y << direction;
    QPoint start(50  + 75 * x, 260 + 75 * y);
    QPoint end = start;

    if (direction == "right") {
        end = end + QPoint(75, 0);
    } else if (direction == "left") {
        end = end + QPoint(-75, 0);
    } else if (direction == "up") {
        end = end + QPoint(0, -75);
    } else if (direction == "down") {
        end = end + QPoint(0, 75);
    }

    QString command = QString("device.drag((%1,%2),(%3,%4), 0.05, 2)\n")
            .arg(start.x()).arg(start.y()).arg(end.x()).arg(end.y());
    qDebug() << command;
    m_monkeyRunnerProcess->write(command.toAscii());
}

void LinePopSolver::makeCommand()
{
    static int count = 0;
    QString command = "";

    if (count % 10 == 0) {
        QString imageFilename = QString("%1.png").arg(count);
        command = command + QString("image = device.takeSnapshot()\n");
        command = command + QString("image.writeToFile(\"%1.png\")\n").arg(count);
        command = command + QString("print (\"image update : %1.png\")\n").arg(count);
    } else {
        int x = rand() % BOARD_WIDTH;
        int y = rand() % BOARD_HEIGHT;
        QString direction = m_directions[rand() % 4];

        QPoint start(50  + 75 * x, 260 + 75 * y);
        QPoint end = start;

        if (direction == "Right") {
            end = end + QPoint(75, 0);
        } else if (direction == "Left") {
            end = end + QPoint(-75, 0);
        } else if (direction == "Up") {
            end = end + QPoint(0, -75);
        } else if (direction == "Down") {
            end = end + QPoint(0, 75);
        }

 //       command = QString("device.drag((%1,%2),(%3,%4), 0.05, 2)\n")
 //               .arg(start.x()).arg(start.y()).arg(end.x()).arg(end.y());
    }
    count++;

    m_monkeyRunnerProcess->write(command.toAscii());

}

void LinePopSolver::start(QString monkeyRunnerPath)
{
    m_monkeyRunnerProcess->start(monkeyRunnerPath);
}

void LinePopSolver::startSolver()
{
    qDebug() << __PRETTY_FUNCTION__;
    takeSnap();

    m_timer->start();
}

void LinePopSolver::updatedImage(QString filename)
{
    cv::Mat srcImage, dst;
    srcImage = cv::imread(filename.toAscii().data());
    QMap<int, QList<Koma> > komaMap;

    for (int y = 0; y < BOARD_HEIGHT; y++) {
//        QList<> List;
        for (int x = 0; x < BOARD_WIDTH; x++) {
            // ここらへんのマジックナンバーは結構 HTC J で割り出した値。
            // 全然移植性ないYO！！！
            // 微調整がひつようだYO！
            cv::Point start =
                    cv::Point(10 , 223) +                                   // 左上のオブジェクト位置
                    cv::Point(KOMA_WIDTH * x, KOMA_WIDTH * y) +             // スタート位置調整
                    cv::Point(20, 60);                                      // 色取得場所スタート位置オフセット

            cv::Point end =
                    cv::Point(10 , 223) +                                  // 左上のオブジェクト位置
                    cv::Point(KOMA_WIDTH * (x+1), KOMA_WIDTH * (y+1)) +    // スタート位置調整
                    cv::Point(-20, -10);                                   // 内側に寄せる

            cv::Rect roiRect(start, end);
            cv::Mat colorSamplingRoi = srcImage(roiRect);
            cv::Mat dstImage(1, 1, colorSamplingRoi.type());
            cv::resize(colorSamplingRoi, dstImage, dstImage.size(), cv::INTER_CUBIC);
            cv::Mat hsvImage;

            // 似ている色を探すために HSV にしておく
            cv::cvtColor(dstImage, hsvImage, CV_RGB2HSV);


            cv::Vec3b hsv = hsvImage.at<cv::Vec3b>(0,0);
//            qDebug() << "hsv : " << hsv[0] << hsv[1] << hsv[2];
            Koma koma(x, y, hsv[0]);
            komaMap[hsv[0]].append(koma);


            cv::resize(dstImage, colorSamplingRoi, colorSamplingRoi.size(), 0, 0, cv::INTER_CUBIC);

//            cv::rectangle(srcImage,
//                          start + cv::Point(20, 20),
//                          start + cv::Point(KOMA_WIDTH, KOMA_WIDTH) - cv::Point(30, 30),
//                          cv::Scalar(0,0,200), 1, 0);

        }
    }

    QList<int> hList;

    QMap<int, QList<Koma> >::const_iterator komaMapIterator = komaMap.constBegin();
    while (komaMapIterator  != komaMap.constEnd()) {
        hList << komaMapIterator.key();
        //        qDebug() << "\t" << i.key() << i.value().size();
        ++komaMapIterator;
    }

    QList<QList<int> > groupList;
    QSet<int> tempGroup;
    bool groupFlag = false;;
    for (int i = 0; i < hList.size(); i++) {
        if (groupFlag == true) {
            tempGroup << hList[i];
        }
        if (i < hList.size() - 1 && hList[i] + 1 == hList[i + 1] ) {
            groupFlag = true;
            tempGroup << hList[i];
        } else {
            groupFlag = false;
            if (!tempGroup.isEmpty()) {
                groupList << tempGroup.toList();
                tempGroup.clear();
            }
        }
    }
    qDebug() << groupList;



    QList<cv::Scalar> scalars;
    scalars << cv::Scalar(0xFF, 0xFF, 0xFF)
            << cv::Scalar(0xFF, 0xFF, 0x00)
            << cv::Scalar(0x90, 0xEE, 0x90)
            << cv::Scalar(0x00, 0xFF, 0xFF)
            << cv::Scalar(0x13, 0x8B, 0x45)
            << cv::Scalar(0x00, 0xA5, 0xFF)
            << cv::Scalar(0xEE, 0x82, 0xEE)
            << cv::Scalar(0x00, 0x00, 0x00)
            << cv::Scalar(0x00, 0x00, 0x00);
    int counter = 0;

    QList<QList<int> > anarizedArray;
    for (int i = 0; i < 7; i++) {
        QList<int> tempList;
        for (int j = 0; j < 7; j++) {
            tempList.append(-1);
        }
        anarizedArray.append(tempList);
    }
    for ( QList<QList<int> >::const_iterator i = groupList.constBegin();
         i != groupList.constEnd(); ++i ) {
        for (QList<int>::const_iterator j = i->constBegin(); j != i->constEnd(); ++j) {
            QList<Koma> komaList = komaMap[*j];
            for (QList<Koma>::const_iterator k = komaList.constBegin(); k != komaList.constEnd(); ++k) {
                int x = k->pos().x();
                int y = k->pos().y();
                anarizedArray[y][x] = counter + 1;


                cv::Point start =
                        cv::Point(10 , 223) +                                   // 左上のオブジェクト位置
                        cv::Point(KOMA_WIDTH * x, KOMA_WIDTH * y);             // スタート位置調整

                cv::Point end =
                        cv::Point(10 , 223) +                                  // 左上のオブジェクト位置
                        cv::Point(KOMA_WIDTH * (x+1), KOMA_WIDTH * (y+1));    // スタート位置調整


                cv::rectangle(srcImage,
                          start, end,
                          scalars[counter],  CV_FILLED, 0);
            }
        }
        counter++;
    }


    qDebug() << anarizedArray;
    for (int y = 0; y < 7; y++) {
        for (int x = 0; x < 7; x++) {
            // 1つ右となりのチェック
            int originX = anarizedArray[y].value(x);
            if (originX == 0 || originX == -1) {
                continue;
            }

            int right   = anarizedArray[y].value(x + 1);
            if (right == 0 || right == -1) {
                continue;
            }

            // 右と同じ色の場合
            if (originX ==  right) {
                // 2つ右となりの上
                QList<int> upList = anarizedArray.value(y - 1);
                if (upList.isEmpty()) {
                    continue;
                }
                int right2up = upList.value(x + 2);
                if (right2up == 0 || right2up == -1) {
                    continue;
                }
                 if (originX == right2up) {
                    swap( x + 2, y - 1, "down");
                }

                // 2つ右となりの下
                QList<int> downList = anarizedArray.value(y + 1);
                if (downList.isEmpty()) {
                    continue;
                }
                int right2down = downList.value(x + 2);
                if (right2down == 0 || right2down == -1) {
                    continue;
                }
                if (originX == right2down) {
                    swap( x + 2, y + 1, "up");
                }

                // 3つと右隣り
                int right3 = anarizedArray[y].value(x + 3);
                if (right3 == 0 || right3 == -1) {
                    continue;
                }
                 if (originX == right3) {
                    swap(x + 3, y, "left");
                }
            }


            // 間ちぇっく
            int right2 = anarizedArray[y].value(x + 2);
            if (right2 == 0 || right2 == -1) {
                continue;
            }
            if (originX == right2) {
                qDebug() << "____________" << x << y;
                // 間上
                QList<int> upList = anarizedArray.value(y - 1);
                if (upList.isEmpty()) {
                    continue;
                }
                int middleUp = upList.value(x + 1);
                if (middleUp == 0 || middleUp == -1) {
                    continue;
                }
                if (originX == middleUp) {
                    swap(x + 1, y - 1, "down");
                }
                // 間下
                QList<int> downList = anarizedArray.value(y + 1);
                if (downList.isEmpty()) {
                    continue;
                }
                int middleDown = downList.value(x + 1);
                if (middleDown == 0 || middleDown == -1) {
                    continue;
                }
                if (originX == middleDown) {
                    swap(x + 1, y + 1, "up");
                }
            }
        }
    }



    cv::cvtColor(srcImage, dst, CV_RGB2BGR); //OpenCVの命令でRGBの順番を入れ替える
    QImage img(dst.data, dst.cols, dst.rows, QImage::Format_RGB888);
    emit updateImage(img);
    takeSnap();
}
