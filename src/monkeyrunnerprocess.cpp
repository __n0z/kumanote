#include "monkeyrunnerprocess.h"
#include <QDebug>

MonkeyRunnerProcess::MonkeyRunnerProcess(QObject *parent) :
    QProcess(parent)
{
    connect(this, SIGNAL(readyReadStandardOutput()), this, SLOT(updatedOutput()));
    connect(this, SIGNAL(readyReadStandardError()), this, SLOT(updatedError()));
}

void MonkeyRunnerProcess::start(QString str) {
    QProcess::start(str);
    QString command ="";
    command = command + "from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice\n";
    command = command + "device = MonkeyRunner.waitForConnection()\n";
    command = command + "print \"Connected\"\n";

    write(command.toAscii());
}


void MonkeyRunnerProcess::finishedProcess( int exitCode, QProcess::ExitStatus exitStatus )
{
    Q_UNUSED(exitCode);
    Q_UNUSED(exitStatus);
}

void MonkeyRunnerProcess::updatedOutput()
{
    setReadChannel(QProcess::StandardOutput);
    while(canReadLine()) {
        QByteArray byteArray = readLine();
        QString output(byteArray);
        output =  output.left(output.size() - 1);
        QRegExp r("^image update");
        if (output == "Connected") {
            emit started();
        } else if (r.indexIn(output) != -1) {
            QString image = output.right(output.length() - QString("image update : ").length());
            emit updatedImage(image);
        }
//        qDebug() << byteArray;
    }
}

void MonkeyRunnerProcess::updatedError()
{
    static bool firstFlag = true;
    setReadChannel(QProcess::StandardError);
    while(canReadLine()) {
        if (firstFlag == true) {
            firstFlag = false;
        }
        QByteArray byteArray = readLine();
        qDebug() << byteArray; //monkeyRunnerOutputArea->append(monkeyRunnerOutput);
    }
}
