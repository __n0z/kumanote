#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFileDialog>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_monkeyRunnerApplicationPath()
{
    ui->setupUi(this);
    m_linePopSolver = new LinePopSolver(this);
    m_monkeyRunnerApplicationPath = "/Users/nozomi/Develop/android-sdk-macosx/tools/monkeyrunner";
    connect(m_linePopSolver, SIGNAL(updateImage(QImage)), this, SLOT(updatedImage(QImage)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_monkeyRunnerButton_clicked()
{
    m_linePopSolver->start(m_monkeyRunnerApplicationPath);
}


void MainWindow::updatedImage(QImage image)
{
    qDebug() << __PRETTY_FUNCTION__;
    image = image.scaled(ui->graphicsView->width(), ui->graphicsView->height(), Qt::KeepAspectRatio, Qt::FastTransformation);
    pixmap = QPixmap::fromImage(image);
    scene.addPixmap(pixmap);
    ui->graphicsView->setScene(&scene);
}
