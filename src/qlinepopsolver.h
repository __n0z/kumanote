#ifndef QLINEPOPSOLVER_H
#define QLINEPOPSOLVER_H

#include <QObject>
#include <QTimer>
#include <QStringList>
#include <QPixmap>
#include "MonkeyRunnerProcess.h"

class LinePopSolver : public QObject
{
    Q_OBJECT
public:
    explicit LinePopSolver(QObject *parent = 0);

    void start(QString monkeyRunnerPath);

    void takeSnap();
    void swap(int x, int y, QString direction);
    
signals:
    void commandNotify(QString);
    void updateImage(QString);
    void updateImage(QImage);

public slots:
    void makeCommand();
    void startSolver();
    void updatedImage(QString image);
    
private:
    QTimer* m_timer;
    QStringList m_directions;


    MonkeyRunnerProcess* m_monkeyRunnerProcess;


    const static int MAKE_COMMAND_INTERVAL = 250;
    const static int BOARD_WIDTH  = 7;
    const static int BOARD_HEIGHT = 7;
    const static double KOMA_WIDTH = 74.28;

};

#endif // QLINEPOPSOLVER_H
