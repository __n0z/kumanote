#-------------------------------------------------
#
# Project created by QtCreator 2012-12-11T23:26:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KumanoTE
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qlinepopsolver.cpp \
    monkeyrunnerprocess.cpp \
    koma.cpp

HEADERS  += mainwindow.h \
    qlinepopsolver.h \
    monkeyrunnerprocess.h \
    koma.h

FORMS    += mainwindow.ui

DEPENDPATH += /usr/local/include
INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib/ \
     -lopencv_core \
     -lopencv_imgproc \
     -lopencv_highgui

OBJECTS_DIR = .obj
MOC_DIR = $$OBJECTS_DIR/moc
RCC_DIR = $$OBJECTS_DIR/rcc
UI_DIR  = $$OBJECTS_DIR/ui
