#include "koma.h"

Koma::Koma(int x, int y, int h)
{
    mPos = QPoint(x, y);
    mH   = h;
}

QPoint Koma::pos() const
{
    return mPos;
}

int Koma::h() const
{
    return mH;
}


