#ifndef KOMA_H
#define KOMA_H
#include <QPoint>

class Koma {
public:
    Koma(int x, int y, int h);
    QPoint pos() const;
    int h() const;

private:
    QPoint mPos;
    int    mH;
};


#endif // KOMA_H
