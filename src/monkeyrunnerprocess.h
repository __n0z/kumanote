#ifndef MONKEYRUNNERPROCESS_H
#define MONKEYRUNNERPROCESS_H

#include <QProcess>

class MonkeyRunnerProcess : public QProcess
{
    Q_OBJECT
public:
    explicit MonkeyRunnerProcess(QObject *parent = 0);
    void start(QString str);





    
signals:
    void started();
    void updatedImage(QString image);


public slots:
    void finishedProcess( int exitCode, QProcess::ExitStatus exitStatus );
    void updatedOutput();
    void updatedError();
};

#endif // MONKEYRUNNERPROCESS_H
